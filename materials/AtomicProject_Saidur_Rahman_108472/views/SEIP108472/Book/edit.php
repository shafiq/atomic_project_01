<?php 
use src\Bitm\SEIP108472\Book\Book;

require_once "../../../vendor/autoload.php";

$object=new Book();
//echo $object->edit();
//echo "<hr>";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create page </title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </head>
  <body>
      <div class="container">
          <div class="row">
              <div class="col-md-6">
                 <h3>Edit your Book Title :</h3>
                 <?php
          if($_SERVER['REQUEST_METHOD']=='POST'){
            $book_title=$_POST['book_title'];
            $author_name=$_POST['author_name'];
            if(empty($book_title) or empty($author_name) ){
              echo "<h4>Every field should be filup </h4>";
            }
            else{
              if(isset($_REQUEST['id'])){
                      $id=$_REQUEST['id'];
                      
                    }
              $update=$object->update($book_title,$author_name,$id);
              if($update){
                header('location:index.php?message=2');
              }
            }

          }
        ?>
                 <?php 
     
                     if(isset($_REQUEST['id'])){
                      $id=$_REQUEST['id'];
                      
                    }
                      $viewalldata=$object->edit($id); 
                    
                  ?>
		<form action="" method="POST">
                      <level>Title of Book :</level>
                        <input class="form-control" type="text" value="<?php echo $viewalldata['book_title'] ?>" 
                        name="book_title" id="" placeholder="Input Book Title"><br>
                        <level>Author Name :</level>
                        <input class="form-control" type="text" value="<?php echo $viewalldata['author_name'] ?>"
                         name="author_name" id="" placeholder="Input Book Title"><br>
      
                        <input class="btn btn-primary" type="submit" name="submit" value="Edit">
    </form>
                  <a class="btn btn-info" href="index.php">Back</a>
              </div>
          </div>
      </div>

  </body>
</html>