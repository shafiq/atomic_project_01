<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atomic Project</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </head>
  <body>
      <div class="container">
          <div class="row">
              <div class="col-md-4">
                  
              </div>
              <div class="col-md-4">
                  <div style="color:red;font-size: 20px;">
                      <span style="color:black;font-size: 25px;">Atomic Project :</span><br>
                  Name: Saidur Rahman<br>
                  SEIP: 108472 <br>
              
                  </div>
                      <table class="table table-striped">
                          
                          <tr>
                              <td><a href="views/SEIP108472/BirthDay/index.php" class="btn btn-info ">Birthday</a></td>
                              <td><a href="views/SEIP108472/Book/index.php" class="btn btn-info ">Book</a></td>
                          </tr>
                          <tr>
                              <td> <a href="views/SEIP108472/City/index.php" class="btn btn-info ">City</a></td>
                              <td><a href="views/SEIP108472/Email/index.php" class="btn btn-info ">Email</a></td>
                          </tr>
                          <tr>
                              <td><a href="views/SEIP108472/Gender/index.php" class="btn btn-info ">Gender</a></td>
                              <td><a href="views/SEIP108472/Hobby/index.php" class="btn btn-info ">Hobby</a></td>
                          </tr>
                          <tr>
                              <td><a href="views/SEIP108472/ProfilePicture/index.php" class="btn btn-info ">ProfilePicture</a></td>
                              <td><a href="views/SEIP108472/Summary/index.php" class="btn btn-info ">Summary</a></td>
                          </tr>
                          <tr>
                              <td><a href="views/SEIP108472/TermsAndCondition/index.php" class="btn btn-info ">TermsAndCondition</a></td>
                              <td></td>
                          </tr>
                      </table>

        
                  
                  
                  
              </div>
             
              
          </div>
               <div class="col-md-4">
                  
              </div>
      </div>
      </div>
    
  </body>
</html>