
		<?php
		ini_set('display_errors', 0);

		//Using composer to load expected Classes automatically.
		include_once('../vendor/autoload.php');

		//using namespace.
		use App\Classes\Hobby\Hobby;

		//Calling Person class to get Data from database.
		$hobby = new Hobby;

		//Calling index() within Person class to fetch Data from database.
		$hobbies = $hobby->index();

		 ?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="../css/style.css" >
	<link rel="stylesheet" type="text/css" href="../../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="#">Birthday</a>
			<a  class="btn btn-default" href="#">Book</a>
			<a  class="btn btn-default" href="#">City</a>
			<a  class="btn btn-default" href="#">Email</a>
			<a  class="btn btn-default" href="#">Gender</a>
			<a  class="btn btn-default active" href="#">Hobby</a>
			<a  class="btn btn-default" href="#">Profile</a>
			<a  class="btn btn-default" href="#">Student</a>
			<a  class="btn btn-default" href="#">Summery</a>
			<a  class="btn btn-default" href="#">Terms & Conditions</a>
		</div>
</div>

		<div class="row nav-bar">
		<div class="col-sm-5 col-sm-offset-4">
		<a  class="btn btn-default" href="../index.php">Hobby</a>
			<a  class="btn btn-default" href="create.php">Add</a>
			<a class="btn btn-default" href="view.php">View List</a>
			<a class="btn btn-default active" href="edit.php">Edit</a>
			<a class="btn btn-default" href="delete.php">Delete</a>
	</div>

	</div>

	<div class="row">
	<div class="col-sm-8 col-sm-offset-2">


		 <div class="row">
		 	<div clas="col-sm-6 col-sm-offset-3">
		 		<table class="table table-bordered">
		 		<tr>
		 		<th class="text-center">Name</th>
		 		<th class="text-center">Hobby</th>
		 		<th class="text-center">Edit</th>
		 		</tr>
		 		<?php 

		 		//using foreach loop to display data individually.
		 		foreach ($hobbies as $person):
		 		 ?>
		 			<tr>
		 				<td><?php echo $person['name'] ?></td>
		 				<td><?php echo $person['hobby'] ?></td>	
		 				<td>
		 					<form action="update.php" method="post">
		 					<input type="hidden" name="id" value="<?php echo $person['id'] ?>">
		 					<input type="submit" value="Edit" class="btn btn-default">
		 					</form>
		 				</td>
		 			</tr>
		 			<?php 
		 			endforeach;
		 			 ?>


		 		</table>
		 		<p>
 		<a class="btn btn-default" href="create.php">Back To Add</a>
 		</p>
		 	</div>
		 </div>


	</div>
	</div>
</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>