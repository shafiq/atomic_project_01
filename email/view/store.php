

 <!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="../css/style.css" >
	<link rel="stylesheet" type="text/css" href="../../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="#">Birthday</a>
			<a  class="btn btn-default" href="#">Book</a>
			<a  class="btn btn-default" href="#">City</a>
			<a  class="btn btn-default active" href="#">Email</a>
			<a  class="btn btn-default" href="#">Gender</a>
			<a  class="btn btn-default" href="#">Hobby</a>
			<a  class="btn btn-default" href="#">Profile</a>
			<a  class="btn btn-default" href="#">Student</a>
			<a  class="btn btn-default" href="#">Summery</a>
			<a  class="btn btn-default" href="#">Terms & Conditions</a>
		</div>
</div>

	<div class="row nav-bar">
		<div class="col-sm-5 col-sm-offset-4">
		<a  class="btn btn-default" href="#">Email</a>
			<a  class="btn btn-default active" href="create.php">Add</a>
			<a class="btn btn-default" href="view.php">View List</a>
			<a class="btn btn-default" href="edit.php">Edit</a>
			<a class="btn btn-default" href="delete.php">Delete</a>
		</div>
	</div>
	<div class="row">
	<div class="col-sm-4 col-sm-offset-4">

<?php 

	//Using composer to load expected Classes automatically.
	include_once('../vendor/autoload.php');

	//Namespase
	use App\Classes\Email\Email;


	if(!empty($_POST['name']) && !is_numeric($_POST['name']) && !empty($_POST['email']) && !is_numeric($_POST['email'])){

		//input field name assigning to $name.
		$name = $_POST['name'];

		//input field profession assigning to $profession
		$email = $_POST['email'];

		//Calling Person class to handle Data.
		$book = new Email;
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$book->storeUpdate($id, $name, $email);
		}else{
		//Sending data from form to Class.
		$book->store($name, $email);
	}
		//Displaying Success message.
		echo "Name: ".$name."<br/>"."Email: ".$email."<br/>"."Succesfully Stored In Your Database";

	}else{

		//Error Message stored into a variable called $emptty.
		$empty = "Name And Email Field Cannot Be Empty or Numeric";

		//Will display error message if any of the form input field is empty or numeric.
		 echo "<strong>";
		 echo $empty;
		 echo "</strong>";
		

	}
 ?>
 		<p>
 		<a class="btn btn-default" href="create.php">Back To Form</a>
 		<a class="btn btn-default" href="view.php">View Book List</a>
 		</p>

	</div>
	</div>
</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>