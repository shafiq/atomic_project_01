

 <!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="../css/style.css" >
	<link rel="stylesheet" type="text/css" href="../../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="#">Birthday</a>
			<a  class="btn btn-default" href="#">Book</a>
			<a  class="btn btn-default" href="#">City</a>
			<a  class="btn btn-default" href="#">Email</a>
			<a  class="btn btn-default" href="#">Gender</a>
			<a  class="btn btn-default" href="#">Hobby</a>
			<a  class="btn btn-default" href="#">Profile</a>
			<a  class="btn btn-default" href="#">Student</a>
			<a  class="btn btn-default" href="#">Summery</a>
			<a  class="btn btn-default active" href="#">Terms & Conditions</a>
		</div>
</div>

	<div class="row nav-bar">
		<div class="col-sm-5 col-sm-offset-4">
		<a  class="btn btn-default" href="#">Terms</a>
			<a  class="btn btn-default" href="create.php">Add</a>
			<a class="btn btn-default" href="view.php">View List</a>
			<a class="btn btn-default" href="edit.php">Edit</a>
			<a class="btn btn-default" href="delete.php">Delete</a>
		</div>
	</div>

	<div class="row">
	<div class="col-sm-4 col-sm-offset-4">

<?php 
ini_set('display_errors', 0);
	//Using composer to load expected Classes automatically.
	include_once('../vendor/autoload.php');

	//Namespase
	use App\Classes\Terms\Terms;


	if(!empty($_POST['terms']) && !is_numeric($_POST['terms']) && isset($_POST['accept'])){

		$terms = $_POST['terms'];

		$accept = $_POST['accept'];

		//Calling Person class to handle Data.
		$info = new Terms;

		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$info->storeUpdate($id,$terms, $accept);
		}else{
		//Sending data from form to Class.
		$info->store($terms, $accept);
		}


		//Displaying Success message.
		echo "Name: ".$terms."<br/>"."Highest Degree: ".$accept."<br>"."Succesfully Stored In Your Database";

	}else{

		//Error Message stored into a variable called $emptty.
		$empty = "Terms And Conditon Field Cannot Be Empty Even You Must Have to Accept Terms & Conditons.";

		//Will display error message if any of the form input field is empty or numeric.
		 echo "<strong>";
		 echo $empty;
		 echo "</strong>";
		

	}

	

		/*$name = $_POST['name'];
		$profession = $_POST['profession'];

		$person = new Person;

		$person->store($name, $profession);

		echo "Name: ".$name."<br/>"."Profession: ".$profession."<br/>"."Succesfully Stored In Your Database";
		*/
 ?>
 		<p>
 		<a class="btn btn-default" href="create.php">Back To Add</a>
 		<a class="btn btn-default" href="view.php">View List</a>
 		</p>

	</div>
	</div>
</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>