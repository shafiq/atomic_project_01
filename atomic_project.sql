-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2016 at 10:38 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`) VALUES
(1, 'Shafiqul Islam', '25/11/1990'),
(2, 'Talha Jubayer', '12/11/2015'),
(3, 'Talha Jubayer', '12/11/2010'),
(5, 'Mian Zadid Rusdid', '25/11/1994');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `book_title` varchar(255) NOT NULL,
  `book_author` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `book_title`, `book_author`) VALUES
(5, 'PHP', 'John Smith'),
(6, 'JavaScript', 'Ullman'),
(8, 'Web Development', 'John Nesh (The Beautiful Mind)'),
(9, 'JavaScript', 'John Nesh');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`) VALUES
(0, 'Shafiqul Islam', 'Kishoregan'),
(1, 'Talha Jubayer', 'Dhaka'),
(2, 'Anik Hasan', 'Chittagong'),
(3, 'Mian Zadid Rusdid', 'Uttara'),
(5, 'Talha Jubayer', 'Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `name`, `email`) VALUES
(1, 'Shafiqul Islam', 'codershafiq@gmail.com'),
(5, 'Shafiqul Islam', 'codershafiq@gmail.com'),
(6, 'Shafiqul Islam', 'hm.shafiq@live.com'),
(13, 'Himel', 'Himel@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`) VALUES
(1, 'Shafiqul Islam', 'Male'),
(2, 'Fahmida Ahmed', 'Female'),
(3, 'Nawrin Talukdar', 'Female'),
(8, 'Iron', 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hobby` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby`) VALUES
(0, 'Talha Jubayer', 'Cricket'),
(3, 'Shafiqul Islam', 'Football and Tracking'),
(5, 'Mian Zadid Rusdid', 'Fishing');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `name`, `photo`) VALUES
(12, 'Anik Hasan', 'images/5699621168d2a4.99991109.jpg'),
(13, 'Talha Jubayer', 'images/56996217ca5bb5.84804933.jpg'),
(14, 'Anik Hasan', 'images/5699622368a590.75440417.jpg'),
(15, 'Shafiqul Islam', 'images/5699622a6a8bf2.36672863.jpg'),
(16, 'Anik Hasan', 'images/569962318908b5.16635443.jpg'),
(17, 'Anik Hasan', 'images/56996237e1db96.95502379.jpg'),
(21, 'Anik Hasan', 'images/5699627b544301.46103082.jpg'),
(24, 'Anik Hasan', 'images/56996358756895.57167621.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `h_degree` varchar(255) NOT NULL,
  `optional` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `h_degree`, `optional`) VALUES
(10, 'Anik Hasan', 'HSC', 'JavaScript'),
(11, 'Mian Zadid Rusdid', 'Post Graduate', 'Zend Certified PHP Programmer'),
(15, 'Shafiqul Islam', 'HSC', 'JavaScript'),
(16, 'Mian Zadid Rusdid', 'Post Graduate', 'Zend Certified PHP Programmer');

-- --------------------------------------------------------

--
-- Table structure for table `summery`
--

CREATE TABLE `summery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `summery` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summery`
--

INSERT INTO `summery` (`id`, `name`, `summery`) VALUES
(5, 'Cannabis Industry', 'Cannabis for life. Cannabis actually put lives in danger.\r\n'),
(7, 'Anik Hasan', 'Cannabis for life. Cannabis actually put lives in danger. \r\nCannabis for life. Cannabis actually put lives in danger. \r\nCannabis for life. Cannabis actually put lives in danger. \r\nCannabis for life. Cannabis actually put lives in danger. ');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `terms` varchar(255) NOT NULL,
  `accept` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `terms`, `accept`) VALUES
(4, ' Your Terms and Conditions Goes Here.\r\nYour Terms and Conditions Goes Here.', 'accepted'),
(5, '.Your Terms and Conditions Goes Here.', 'accepted'),
(6, ' .Your Terms and Conditions Goes Here.', 'accepted'),
(8, 'You should write down some terms and conditons!', 'accepted');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summery`
--
ALTER TABLE `summery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `summery`
--
ALTER TABLE `summery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
