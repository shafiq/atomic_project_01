<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="../css/style.css" >
	<link rel="stylesheet" type="text/css" href="../../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="#">Birthday</a>
			<a  class="btn btn-default active" href="#">Book</a>
			<a  class="btn btn-default" href="#">City</a>
			<a  class="btn btn-default" href="#">Email</a>
			<a  class="btn btn-default" href="#">Gender</a>
			<a  class="btn btn-default" href="#">Hobby</a>
			<a  class="btn btn-default" href="#">Profile</a>
			<a  class="btn btn-default" href="#">Student</a>
			<a  class="btn btn-default" href="#">Summery</a>
			<a  class="btn btn-default" href="#">Terms & Conditions</a>
		</div>
</div>

	<div class="row nav-bar">
		<div class="col-sm-4 col-sm-offset-4">
		<a  class="btn btn-default active" href="#">Book</a>
			<a  class="btn btn-default" href="create.php">Add</a>
			<a class="btn btn-default" href="view.php">View List</a>
			<a class="btn btn-default active" href="edit.php">Edit</a>
			<a class="btn btn-default" href="delete.php">Delete</a>
		</div>
	</div>

	<div class="row">
	<div class="col-sm-8 col-sm-offset-2">

		<?php
		ini_set('display_errors', 0);
		//Using composer to load expected Classes automatically.
		include_once('../vendor/autoload.php');

		//using namespace.
		use App\Classes\Book\Book;

		//Calling Person class to get Data from database.
		$book = new Book;

		//Calling index() within Person class to fetch Data from database.
		$books = $book->index();

		 ?>

		 <div class="row">
		 	<div clas="col-sm-6 col-sm-offset-3">
		 		<table class="table table-bordered">
		 		<tr>
		 		<th>Book Title</th>
		 		<th>Book Author</th>
		 		<th>Edit</th>
		 		</tr>
		 		<?php 

		 		//using foreach loop to display data individually.
		 		foreach ($books as $book):
		 		 ?>
		 			<tr>
		 				<td><?php echo $book['book_title'] ?></td>
		 				<td><?php echo $book['book_author'] ?></td>	
		 				<td>
		 					<form action="update.php" method="post">
		 					<input type="hidden" name="id" value="<?php echo $book['id'] ?>">
		 					<input type="submit" value="Edit" class="btn btn-default">
		 					</form>
		 				</td>
		 			</tr>
		 			<?php 
		 			endforeach;
		 			 ?>


		 		</table>
		 		<p>
 		<a class="btn btn-default" href="create.php">Back To Form</a>
 		</p>
		 	</div>
		 </div>


	</div>
	</div>
</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>