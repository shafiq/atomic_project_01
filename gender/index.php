<!DOCTYPE html>
<html>
<head>
	<title>Submission Form</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="css/style.css" >
	<link rel="stylesheet" type="text/css" href="../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="../birthday/index.php">Birthday</a>
			<a  class="btn btn-default" href="../book/index.php">Book</a>
			<a  class="btn btn-default" href="../city/index.php">City</a>
			<a  class="btn btn-default" href="../email/index.php">Email</a>
			<a  class="btn btn-default active" href="#">Gender</a>
			<a  class="btn btn-default" href="../hobby/index.php">Hobby</a>
			<a  class="btn btn-default" href="../profile/index.php">Profile</a>
			<a  class="btn btn-default" href="../student/index.php">Student</a>
			<a  class="btn btn-default" href="../summery/index.php">Summery</a>
			<a  class="btn btn-default" href="../terms/index.php">Terms & Conditions</a>
		</div>
	</div>

	<div class="row nav-bar">
		<div class="col-sm-5 col-sm-offset-4">
			<a  class="btn btn-default active" href="index.php">Gender</a>
			<a  class="btn btn-default" href="view/create.php">Add</a>
			<a class="btn btn-default" href="view/view.php">View List</a>
			<a class="btn btn-default" href="view/edit.php">Edit</a>
			<a class="btn btn-default" href="view/delete.php">Delete</a>
		</div>
	</div>


</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>