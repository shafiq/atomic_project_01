

 <!DOCTYPE html>
<html>
<head>
	<title><title>Data Collection Form</title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="../css/style.css" >
	<link rel="stylesheet" type="text/css" href="../../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="#">Birthday</a>
			<a  class="btn btn-default" href="#">Book</a>
			<a  class="btn btn-default" href="#">City</a>
			<a  class="btn btn-default" href="#">Email</a>
			<a  class="btn btn-default active" href="#">Gender</a>
			<a  class="btn btn-default" href="#">Hobby</a>
			<a  class="btn btn-default" href="#">Profile</a>
			<a  class="btn btn-default" href="#">Student</a>
			<a  class="btn btn-default" href="#">Summery</a>
			<a  class="btn btn-default" href="#">Terms & Conditions</a>
		</div>
</div>

<div class="row nav-bar">
		<div class="col-sm-5 col-sm-offset-4">
		<a  class="btn btn-default" href="#">Gender</a>
			<a  class="btn btn-default" href="create.php">Add</a>
			<a class="btn btn-default" href="view.php">View List</a>
			<a class="btn btn-default" href="edit.php">Edit</a>
			<a class="btn btn-default" href="delete.php">Delete</a>
		</div>
	</div>

	<div class="row">
	<div class="col-sm-4 col-sm-offset-4">

<?php 
ini_set('display_errors', 0);
	//Using composer to load expected Classes automatically.
	include_once('../vendor/autoload.php');

	//Namespase
	use App\Classes\Gender\Gender;


	if(!empty($_POST['name']) && !is_numeric($_POST['name']) && !empty($_POST['gender']) && !is_numeric($_POST['gender'])){

		//input field name assigning to $name.
		$name = $_POST['name'];


		//input field profession assigning to $profession
		$gender = $_POST['gender'];

		//Calling Person class to handle Data.
		$persons = new Gender;

		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$persons->storeUpdate($id, $name, $gender);
			// var_dump($persons);
		}else{
		//Sending data from form to Class.
		$persons->store($name, $gender);
		}

		//Sending data from form to Class.
		

		//Displaying Success message.
		echo "Name: ".$name."<br/>"."Gender: ".$gender."<br/>"."Succesfully Stored In Your Database";

	}else{

		//Error Message stored into a variable called $emptty.
		$empty = "Name And Gender Field Cannot Be Empty or Numeric";

		//Will display error message if any of the form input field is empty or numeric.
		 echo "<strong>";
		 echo $empty;
		 echo "</strong>";
		

	}
 ?>
 		<p>
 		<a class="btn btn-default" href="create.php">Back To Add</a>
 		<a class="btn btn-default" href="view.php">View List</a>
 		</p>

	</div>
	</div>
</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>