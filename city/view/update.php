<!DOCTYPE html>
<html>
<head>
	<title>Submission Form</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="../css/style.css" >
	<link rel="stylesheet" type="text/css" href="../../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="#">Birthday</a>
			<a  class="btn btn-default" href="#">Book</a>
			<a  class="btn btn-default active" href="#">City</a>
			<a  class="btn btn-default" href="#">Email</a>
			<a  class="btn btn-default" href="#">Gender</a>
			<a  class="btn btn-default" href="#">Hobby</a>
			<a  class="btn btn-default" href="#">Profile</a>
			<a  class="btn btn-default" href="#">Student</a>
			<a  class="btn btn-default" href="#">Summery</a>
			<a  class="btn btn-default" href="#">Terms & Conditions</a>
		</div>
</div>

	<div class="row nav-bar">
		<div class="col-sm-5 col-sm-offset-4">
		<a  class="btn btn-default active" href="#">City</a>
			<a  class="btn btn-default" href="create.php">Add</a>
			<a class="btn btn-default" href="view.php">View List</a>
			<a class="btn btn-default" href="edit.php">Edit</a>
			<a class="btn btn-default" href="delete.php">Delete</a>
		</div>
	</div>
	<?php 
	ini_set('display_errors', 0);
		include_once('../vendor/autoload.php');

		$id = $_POST['id'];
		//echo "$id";

		//using namespace.
		use App\Classes\City\City;

		//Calling Person class to get Data from database.
		$city = new city;

		//Calling index() within Person class to fetch Data from database.
		$cities = $city->update($id);

 	?>
	<div class="row">
		<div class="col-sm-4 col-sm-offset-4">
			<form action="store.php" method="post">
				<fieldset>
				<div class="form-group">
					<legend>Enter Your Personal Information</legend>
					
					<label>Name:</label>
					<input type="text" class="form-control" name="name" required value="<?php echo $cities['name'] ?>">
					<label>City</label>
					<input type="text" class="form-control" name="city"  value="<?php echo $cities['city'] ?>" required>
					<input type="hidden" class="form-control" name="id"  value="<?php echo $cities['id'] ?>" required>

	
					</div>
					</legend>
				</fieldset>

				<input type="submit" class="btn btn-default" value="Save">

			</form>
		</div>

	</div>


</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>