

 <!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="../css/style.css" >
		<link rel="stylesheet" type="text/css" href="../../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="#">Birthday</a>
			<a  class="btn btn-default" href="#">Book</a>
			<a  class="btn btn-default" href="#">City</a>
			<a  class="btn btn-default" href="#">Email</a>
			<a  class="btn btn-default" href="#">Gender</a>
			<a  class="btn btn-default" href="#">Hobby</a>
			<a  class="btn btn-default active" href="#">Profile</a>
			<a  class="btn btn-default" href="#">Student</a>
			<a  class="btn btn-default" href="#">Summery</a>
			<a  class="btn btn-default" href="#">Terms & Conditions</a>
		</div>
</div>

	<div class="row nav-bar">
		<div class="col-sm-5 col-sm-offset-4">
		<a  class="btn btn-default active" href="#">Profile</a>
			<a  class="btn btn-default" href="create.php">Add</a>
			<a class="btn btn-default" href="view.php">View List</a>
			<a class="btn btn-default" href="edit.php">Edit</a>
			<a class="btn btn-default" href="delete.php">Delete</a>
		</div>
	</div>

	<div class="row">
	<div class="col-sm-4 col-sm-offset-4">

<?php 
	ini_set('display_errors', 0);
	include_once('../vendor/autoload.php');
	use App\Classes\Profile\Profile;

		if (isset($_FILES['file']) && isset($_POST['name'])) {
			$file = $_FILES['file'];
			$name = $_POST['name'];

			// print_r($file);

			$file_name = $file['name'];
			$file_tmp = $file['tmp_name'];
			$file_size = $file['size'];
			$file_error = $file['error'];



			//Properties
			$file_ext = explode('.', $file_name);

			$file_ext = strtolower(end($file_ext));
			$allowed = array('jpg', 'jpeg', 'png' );

			if (in_array($file_ext, $allowed)) {
				
				if ($file_error === 0) {
					if ($file_size <= 4097152) {
						
						$file_new_name = uniqid('', true) . '.' . $file_ext;
						$file_destinaton = 'images/'.$file_new_name;

						if (move_uploaded_file($file_tmp, $file_destinaton)) {

							$file_destinaton;
							$object = new Profile;

							if(isset($_POST['id'])){
								$id = $_POST['id'];
								$object->storeUpdate($id, $name, $file_destinaton);
							}else{
							//Sending data from form to Class.
							$object->store($name, $file_destinaton);
							}


							echo "Successfully Stored";
					}
				}
			}
		}
	}else{
		echo "Name and Image Cannot Be Empty!";

	}
		

	// 	$object = new Profile;

	// 	if(isset($_POST['id']) && ){
	// 		$id = $_POST['id'];
	// 		$object->storeUpdate($id, $name, $file_destinaton);
	// 	}else{
	// 	//Sending data from form to Class.
	// 	$object->store($name, $file_destinaton);
	// }

		


 ?>	
 		<p>
 		<a class="btn btn-default" href="create.php">Back To Form</a>
 		<a class="btn btn-default" href="view.php">View Images</a>
 		</p>


	</div>
	</div>
</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>