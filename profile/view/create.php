<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" >
	<link rel="stylesheet" type="text/css" href="../css/style.css" >
		<link rel="stylesheet" type="text/css" href="../../css/index.css" >
</head>
<body>
<div class="container-fluid">
<div class="row index-nav">
		<div class="col-sm-9 col-sm-offset-2">
			<a  class="btn btn-default" href="../../index.php">Assignment Home</a>
			<a  class="btn btn-default" href="#">Birthday</a>
			<a  class="btn btn-default" href="#">Book</a>
			<a  class="btn btn-default" href="#">City</a>
			<a  class="btn btn-default" href="#">Email</a>
			<a  class="btn btn-default" href="#">Gender</a>
			<a  class="btn btn-default" href="#">Hobby</a>
			<a  class="btn btn-default active" href="#">Profile</a>
			<a  class="btn btn-default" href="#">Student</a>
			<a  class="btn btn-default" href="#">Summery</a>
			<a  class="btn btn-default" href="#">Terms & Conditions</a>
		</div>
</div>

	<div class="row nav-bar">
		<div class="col-sm-5 col-sm-offset-4">
		<a  class="btn btn-default active" href="#">Profile</a>
			<a  class="btn btn-default active" href="create.php">Add</a>
			<a class="btn btn-default" href="view.php">View List</a>
			<a class="btn btn-default" href="edit.php">Edit</a>
			<a class="btn btn-default" href="delete.php">Delete</a>
		</div>
	</div>

	<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
	<form action="store.php" method="post" enctype="multipart/form-data">
		<div class="form-group">
		<legend>Enter Your Full Name and Select Your Photograph</legend>
			<label>Name:</label>
			<input type="text" class="form-control" name="name" required="required">
			<br>
			<label>Select Image File ( 'jpg', 'jpeg' and 'png' only allowed)</label>
			<input type="file" name="file">
			<input type="submit" value="Upload">
		</div>
	</form>

	</div>
	</div>
</div>


<script src="bootstrap.js"></script
<script src="bootstrap.min.js"></script

</body>
</html>